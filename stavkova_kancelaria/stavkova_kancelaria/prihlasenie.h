﻿#pragma once
#ifndef PRIHLASENIE_HPP
#define PRIHLASENIE_HPP

#include <QDialog>
#include "ui_prihlasenie.h"


class prihlasenie : public QDialog {
	Q_OBJECT

public:
	prihlasenie(QWidget * parent = Q_NULLPTR);
	~prihlasenie();

	QString zadaj_meno();
	QString zadaj_heslo();

	public slots:
	void prihl_ok();

private:
	Ui::prihlasenie ui;

	QString meno;
	QString heslo;

};
#endif // PRIHLASENIE_HPP
﻿#include "prihlasenie.h"
#include <stavka.h>

prihlasenie::prihlasenie(QWidget * parent) : QDialog(parent) {
	ui.setupUi(this);
}

prihlasenie::~prihlasenie() {
	
}

void prihlasenie::prihl_ok()
{
	stavka stav;

	stav.exec();
}

QString prihlasenie::zadaj_meno()
{
	return meno;
}

QString prihlasenie::zadaj_heslo()
{
	return heslo;
}

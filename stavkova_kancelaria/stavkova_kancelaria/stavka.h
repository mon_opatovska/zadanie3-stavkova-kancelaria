﻿#pragma once
#ifndef STAVKA_HPP
#define STAVKA_HPP

#include <QDialog>
#include "ui_stavka.h"

class stavka : public QDialog {
	Q_OBJECT

public:
	stavka(QWidget * parent = Q_NULLPTR);
	~stavka();

	public slots:
	void ok_stav();

private:
	Ui::stavka ui;
};
#endif //STAVKA_HPP

/********************************************************************************
** Form generated from reading UI file 'stavkova_kancelaria.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_STAVKOVA_KANCELARIA_H
#define UI_STAVKOVA_KANCELARIA_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_stavkova_kancelariaClass
{
public:
    QWidget *centralWidget;
    QLabel *label;
    QPushButton *pushButton;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *stavkova_kancelariaClass)
    {
        if (stavkova_kancelariaClass->objectName().isEmpty())
            stavkova_kancelariaClass->setObjectName(QStringLiteral("stavkova_kancelariaClass"));
        stavkova_kancelariaClass->resize(443, 338);
        centralWidget = new QWidget(stavkova_kancelariaClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(80, 30, 281, 41));
        QFont font;
        font.setPointSize(12);
        label->setFont(font);
        label->setAlignment(Qt::AlignCenter);
        pushButton = new QPushButton(centralWidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(170, 130, 101, 41));
        stavkova_kancelariaClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(stavkova_kancelariaClass);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 443, 21));
        stavkova_kancelariaClass->setMenuBar(menuBar);
        mainToolBar = new QToolBar(stavkova_kancelariaClass);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        stavkova_kancelariaClass->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(stavkova_kancelariaClass);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        stavkova_kancelariaClass->setStatusBar(statusBar);

        retranslateUi(stavkova_kancelariaClass);
        QObject::connect(pushButton, SIGNAL(clicked()), stavkova_kancelariaClass, SLOT(klik()));

        QMetaObject::connectSlotsByName(stavkova_kancelariaClass);
    } // setupUi

    void retranslateUi(QMainWindow *stavkova_kancelariaClass)
    {
        stavkova_kancelariaClass->setWindowTitle(QApplication::translate("stavkova_kancelariaClass", "stavkova_kancelaria", 0));
        label->setText(QApplication::translate("stavkova_kancelariaClass", "Vitajte v st\303\241vkovej kancel\303\241rii", 0));
        pushButton->setText(QApplication::translate("stavkova_kancelariaClass", "Prihl\303\241si\305\245 sa", 0));
    } // retranslateUi

};

namespace Ui {
    class stavkova_kancelariaClass: public Ui_stavkova_kancelariaClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_STAVKOVA_KANCELARIA_H

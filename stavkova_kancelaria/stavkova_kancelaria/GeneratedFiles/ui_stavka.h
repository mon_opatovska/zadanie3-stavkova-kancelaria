/********************************************************************************
** Form generated from reading UI file 'stavka.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_STAVKA_H
#define UI_STAVKA_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>

QT_BEGIN_NAMESPACE

class Ui_stavka
{
public:
    QLabel *label;
    QLabel *label_2;
    QComboBox *comboBox;
    QLabel *label_3;
    QGroupBox *groupBox;
    QRadioButton *radioButton;
    QRadioButton *radioButton_2;
    QRadioButton *radioButton_3;
    QLabel *label_4;
    QLineEdit *lineEdit;
    QComboBox *comboBox_2;
    QGroupBox *groupBox_2;
    QRadioButton *radioButton_4;
    QRadioButton *radioButton_5;
    QRadioButton *radioButton_6;
    QLineEdit *lineEdit_2;
    QComboBox *comboBox_3;
    QGroupBox *groupBox_3;
    QRadioButton *radioButton_7;
    QRadioButton *radioButton_8;
    QRadioButton *radioButton_9;
    QLineEdit *lineEdit_3;
    QPushButton *pushButton;

    void setupUi(QDialog *stavka)
    {
        if (stavka->objectName().isEmpty())
            stavka->setObjectName(QStringLiteral("stavka"));
        stavka->resize(560, 436);
        label = new QLabel(stavka);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(10, 0, 181, 31));
        QFont font;
        font.setPointSize(10);
        label->setFont(font);
        label_2 = new QLabel(stavka);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(20, 40, 261, 91));
        comboBox = new QComboBox(stavka);
        comboBox->setObjectName(QStringLiteral("comboBox"));
        comboBox->setGeometry(QRect(20, 180, 111, 22));
        label_3 = new QLabel(stavka);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(20, 160, 47, 13));
        label_3->setFont(font);
        groupBox = new QGroupBox(stavka);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        groupBox->setGeometry(QRect(160, 160, 241, 51));
        groupBox->setFont(font);
        radioButton = new QRadioButton(groupBox);
        radioButton->setObjectName(QStringLiteral("radioButton"));
        radioButton->setGeometry(QRect(10, 30, 82, 17));
        radioButton_2 = new QRadioButton(groupBox);
        radioButton_2->setObjectName(QStringLiteral("radioButton_2"));
        radioButton_2->setGeometry(QRect(80, 30, 61, 17));
        radioButton_3 = new QRadioButton(groupBox);
        radioButton_3->setObjectName(QStringLiteral("radioButton_3"));
        radioButton_3->setGeometry(QRect(160, 30, 61, 17));
        label_4 = new QLabel(stavka);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(420, 160, 47, 13));
        label_4->setFont(font);
        lineEdit = new QLineEdit(stavka);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));
        lineEdit->setGeometry(QRect(420, 180, 113, 20));
        comboBox_2 = new QComboBox(stavka);
        comboBox_2->setObjectName(QStringLiteral("comboBox_2"));
        comboBox_2->setGeometry(QRect(20, 220, 111, 21));
        groupBox_2 = new QGroupBox(stavka);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        groupBox_2->setGeometry(QRect(160, 210, 241, 41));
        radioButton_4 = new QRadioButton(groupBox_2);
        radioButton_4->setObjectName(QStringLiteral("radioButton_4"));
        radioButton_4->setGeometry(QRect(10, 20, 82, 17));
        radioButton_4->setFont(font);
        radioButton_5 = new QRadioButton(groupBox_2);
        radioButton_5->setObjectName(QStringLiteral("radioButton_5"));
        radioButton_5->setGeometry(QRect(80, 20, 82, 17));
        radioButton_5->setFont(font);
        radioButton_6 = new QRadioButton(groupBox_2);
        radioButton_6->setObjectName(QStringLiteral("radioButton_6"));
        radioButton_6->setGeometry(QRect(160, 20, 82, 17));
        radioButton_6->setFont(font);
        lineEdit_2 = new QLineEdit(stavka);
        lineEdit_2->setObjectName(QStringLiteral("lineEdit_2"));
        lineEdit_2->setGeometry(QRect(420, 220, 113, 20));
        comboBox_3 = new QComboBox(stavka);
        comboBox_3->setObjectName(QStringLiteral("comboBox_3"));
        comboBox_3->setGeometry(QRect(20, 260, 111, 21));
        groupBox_3 = new QGroupBox(stavka);
        groupBox_3->setObjectName(QStringLiteral("groupBox_3"));
        groupBox_3->setGeometry(QRect(160, 250, 241, 41));
        radioButton_7 = new QRadioButton(groupBox_3);
        radioButton_7->setObjectName(QStringLiteral("radioButton_7"));
        radioButton_7->setGeometry(QRect(10, 20, 82, 17));
        radioButton_7->setFont(font);
        radioButton_8 = new QRadioButton(groupBox_3);
        radioButton_8->setObjectName(QStringLiteral("radioButton_8"));
        radioButton_8->setGeometry(QRect(80, 20, 82, 17));
        radioButton_8->setFont(font);
        radioButton_9 = new QRadioButton(groupBox_3);
        radioButton_9->setObjectName(QStringLiteral("radioButton_9"));
        radioButton_9->setGeometry(QRect(160, 20, 82, 16));
        radioButton_9->setFont(font);
        lineEdit_3 = new QLineEdit(stavka);
        lineEdit_3->setObjectName(QStringLiteral("lineEdit_3"));
        lineEdit_3->setGeometry(QRect(420, 260, 113, 20));
        pushButton = new QPushButton(stavka);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(240, 350, 75, 23));

        retranslateUi(stavka);
        QObject::connect(pushButton, SIGNAL(clicked()), stavka, SLOT(ok_stav()));

        QMetaObject::connectSlotsByName(stavka);
    } // setupUi

    void retranslateUi(QDialog *stavka)
    {
        stavka->setWindowTitle(QApplication::translate("stavka", "stavka", 0));
        label->setText(QApplication::translate("stavka", "Dnes sa hraj\303\272 z\303\241pasy", 0));
        label_2->setText(QApplication::translate("stavka", "text", 0));
        comboBox->clear();
        comboBox->insertItems(0, QStringList()
         << QApplication::translate("stavka", "Nov\303\241 polo\305\276ka", 0)
         << QApplication::translate("stavka", "Nov\303\241 polo\305\276ka1", 0)
         << QApplication::translate("stavka", "Nov\303\241 polo\305\276ka2", 0)
        );
        label_3->setText(QApplication::translate("stavka", "z\303\241pas", 0));
        groupBox->setTitle(QApplication::translate("stavka", "tip", 0));
        radioButton->setText(QApplication::translate("stavka", "prv\303\275", 0));
        radioButton_2->setText(QApplication::translate("stavka", "druh\303\275", 0));
        radioButton_3->setText(QApplication::translate("stavka", "rem\303\255za", 0));
        label_4->setText(QApplication::translate("stavka", "suma", 0));
        comboBox_2->clear();
        comboBox_2->insertItems(0, QStringList()
         << QApplication::translate("stavka", "Nov\303\241 polo\305\276ka", 0)
         << QApplication::translate("stavka", "Nov\303\241 polo\305\276ka1", 0)
         << QApplication::translate("stavka", "Nov\303\241 polo\305\276ka2", 0)
        );
        groupBox_2->setTitle(QString());
        radioButton_4->setText(QApplication::translate("stavka", "prv\303\275", 0));
        radioButton_5->setText(QApplication::translate("stavka", "druh\303\275", 0));
        radioButton_6->setText(QApplication::translate("stavka", "rem\303\255za", 0));
        comboBox_3->clear();
        comboBox_3->insertItems(0, QStringList()
         << QApplication::translate("stavka", "Nov\303\241 polo\305\276ka", 0)
         << QApplication::translate("stavka", "Nov\303\241 polo\305\276ka1", 0)
         << QApplication::translate("stavka", "Nov\303\241 polo\305\276ka2", 0)
        );
        groupBox_3->setTitle(QString());
        radioButton_7->setText(QApplication::translate("stavka", "prv\303\275", 0));
        radioButton_8->setText(QApplication::translate("stavka", "druh\303\275", 0));
        radioButton_9->setText(QApplication::translate("stavka", "rem\303\255za", 0));
        pushButton->setText(QApplication::translate("stavka", "OK", 0));
    } // retranslateUi

};

namespace Ui {
    class stavka: public Ui_stavka {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_STAVKA_H

/********************************************************************************
** Form generated from reading UI file 'prihlasenie.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PRIHLASENIE_H
#define UI_PRIHLASENIE_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_prihlasenie
{
public:
    QLabel *label;
    QLineEdit *lineEdit;
    QLineEdit *lineEdit_2;
    QLabel *label_2;
    QPushButton *pushButton;

    void setupUi(QDialog *prihlasenie)
    {
        if (prihlasenie->objectName().isEmpty())
            prihlasenie->setObjectName(QStringLiteral("prihlasenie"));
        prihlasenie->resize(273, 191);
        label = new QLabel(prihlasenie);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(36, 42, 61, 21));
        lineEdit = new QLineEdit(prihlasenie);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));
        lineEdit->setGeometry(QRect(100, 40, 113, 20));
        lineEdit_2 = new QLineEdit(prihlasenie);
        lineEdit_2->setObjectName(QStringLiteral("lineEdit_2"));
        lineEdit_2->setGeometry(QRect(100, 80, 113, 20));
        label_2 = new QLabel(prihlasenie);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(36, 80, 51, 20));
        pushButton = new QPushButton(prihlasenie);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(90, 130, 75, 23));

        retranslateUi(prihlasenie);
        QObject::connect(pushButton, SIGNAL(clicked()), prihlasenie, SLOT(prihl_ok()));

        QMetaObject::connectSlotsByName(prihlasenie);
    } // setupUi

    void retranslateUi(QDialog *prihlasenie)
    {
        prihlasenie->setWindowTitle(QApplication::translate("prihlasenie", "prihlasenie", 0));
        label->setText(QApplication::translate("prihlasenie", "Meno", 0));
        label_2->setText(QApplication::translate("prihlasenie", "Heslo", 0));
        pushButton->setText(QApplication::translate("prihlasenie", "OK", 0));
    } // retranslateUi

};

namespace Ui {
    class prihlasenie: public Ui_prihlasenie {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PRIHLASENIE_H

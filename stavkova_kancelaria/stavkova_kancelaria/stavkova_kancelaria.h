#ifndef STAVKOVA_KANCELARIA_H
#define STAVKOVA_KANCELARIA_H

#include <QtWidgets/QMainWindow>
#include "ui_stavkova_kancelaria.h"
#include <prihlasenie.h>
#include <stavka.h>
#include <QMessageBox>

class stavkova_kancelaria : public QMainWindow
{
	Q_OBJECT

public:
	stavkova_kancelaria(QWidget *parent = 0);
	~stavkova_kancelaria();
	
	public slots:
	void klik();


private:
	Ui::stavkova_kancelariaClass ui;
};

#endif // STAVKOVA_KANCELARIA_H
